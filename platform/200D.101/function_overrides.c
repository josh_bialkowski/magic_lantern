// functions 200D/101

// SJE overrides for functions I couldn't find stubs for,
// as well as ones that I think function sig has changed,
// so we can define wrappers.

#include <dryos.h>
#include <property.h>
#include <bmp.h>
#include <config.h>
#include <consts.h>
#include <lens.h>

#include "log-d678.h"
#include "bootflags.h"

// This first set of functions is for wrapping
// instances where I (SJE) believe the function
// sig has changed between what ML expects and
// what 200D uses.

void LoadCalendarFromRTC(struct tm *tm)
{
    bmp_printf_auto("LoadCalendarFromRTC called == we're maybe screwed");
    _LoadCalendarFromRTC(tm, 0, 0, 0, 0);
}

// Doesn't look like Canon or ML ever use the return value,
// perhaps why sig has changed between 50D and 200D.
// We wrap this to be safe.
extern void _AddMemoryChunk(struct memSuite *suite, struct memChunk *chunk);
int AddMemoryChunk(struct memSuite *suite, struct memChunk *chunk)
{
    bmp_printf_auto("AddMemoryChunk called == we're maybe screwed");
    _AddMemoryChunk(suite, chunk);
    return 0;
}



// This set is for functions that 200D doesn't call Canon for,
// instead implementing directly.

void fsuDecodePartitionTable(const uint8_t *partition_entry, struct partition_table *info)
{
    info->state = partition_entry[0];
    info->sectors_before_partition = *(uint32_t *) &partition_entry[8];
    info->sectors_in_partition = *(uint32_t *) &partition_entry[12];
}




// I thought I needed these stubs, but I think I don't yet?
#if 0
void SetEDmac(unsigned int channel, void *address, struct edmac_info *ptr, int flags)
{
    return;
}

void ConnectWriteEDmac(unsigned int channel, unsigned int where)
{
    return;
}

void ConnectReadEDmac(unsigned int channel, unsigned int where)
{
    return;
}

void StartEDmac(unsigned int channel, int flags)
{
    return;
}

void AbortEDmac(unsigned int channel)
{
    return;
}

void RegisterEDmacCompleteCBR(int channel, void (*cbr)(void*), void* cbr_ctx)
{
    return;
}

void UnregisterEDmacCompleteCBR(int channel)
{
    return;
}

void RegisterEDmacAbortCBR(int channel, void (*cbr)(void*), void* cbr_ctx)
{
    return;
}

void UnregisterEDmacAbortCBR(int channel)
{
    return;
}

void RegisterEDmacPopCBR(int channel, void (*cbr)(void*), void* cbr_ctx)
{
    return;
}

void UnregisterEDmacPopCBR(int channel)
{
    return;
}
#endif





// This set are stubs I haven't found yet.
// We try and log if they get called while
// hoping they're not yet needed.
void _EngDrvOut(uint32_t reg, uint32_t value)
{
    bmp_printf_auto("_EngDrvOut called == we're screwed");
    return;
}

uint32_t shamem_read(uint32_t addr)
{
    bmp_printf_auto("shamem_read called == we're screwed");
    return 0;
}

void _engio_write(uint32_t* reg_list)
{
    bmp_printf_auto("_engio_write called == we're screwed");
    return;
}

unsigned int UnLockEngineResources()
{
    bmp_printf_auto("_UnLockEngineResources called == we're screwed");
    return 0;
}
