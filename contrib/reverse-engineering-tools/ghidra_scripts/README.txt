Ghidra expects to find scripts in ~/ghidra_scripts
Copy the scripts you want to use to that location.

If you are making a new script, please include this line:
#@category Magiclantern

That way, all Magiclantern scripts are in the same place
in Ghidra.  Thanks!
