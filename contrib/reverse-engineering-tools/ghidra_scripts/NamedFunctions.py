#Loads a named_functions.idc, finds function stubs, disassembles and labels those addresses
#@author SJE
#@category Magiclantern
#@keybinding 
#@menupath 
#@toolbar 

def stub_namer(address, name):
    print("Disassembling '%s' at %s" % (name, address.toString()))
    removeFunctionAt(address)
    removeSymbol(address, name)
    clearListing(address)
    # TODO we should set Thumb bit appropriately based on address,
    # see MakeAutoNamedFunc in named_functions.idc for the IDA way
    createFunction(address, name)
    disassemble(address)


try:
    stubs_path = askFile("Select named_functions.idc", "OK").getAbsolutePath()
except ghidra.util.exception.CancelledException:
    exit()

# The following may not work on Windows, we may be opening the file twice.
stubs = [] # will be list of (address, name) tuples
bad_addresses = [] # stub lines we didn't understand
with open(stubs_path, "r") as f:
    for line in f.readlines():
        # assume functions live in the 0x10000000-0xffffffff region, eg
        # NSTUB(0xFF86DD88,  take_semaphore)
        if line.startswith("  MakeAutoNamedFunc("):
            address = line.split(",")[0].split("(")[1].strip()
            name = line.split(",")[1].split(")")[0].strip().strip('"')
            try:
                address_int = int(address, 16) & ~1;
            except ValueError:
                bad_addresses.append(address)
                continue
            stubs.append((toAddr(address_int), name))

for s in stubs:
    stub_namer(s[0], s[1])

if bad_addresses:
    print("Some functions weren't understood:")
    for a in bad_addresses:
        print(a)
